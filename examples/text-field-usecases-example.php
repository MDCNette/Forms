<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);
if (@!include __DIR__ . '/../vendor/autoload.php') {
    die('Install packages using `composer install`');
}

use MDCNette\Forms\MDCForm;
use Nette\Utils\Html;
use Tracy\Debugger;
use Tracy\Dumper;

Debugger::enable();
$form = new MDCForm();
$renderer = $form->getRenderer();
$renderer->wrappers['controls']['container'] = 'div class=text-field-row';

// group Personal data
$form->addGroup('Validation');
$form->addText('filled1', 'Standard')->setHelperText('Helper Text', true)->setRequired();
$form->addText('filled2', 'Standard')->setHelperText('Helper Text', true)
    ->setIcon('event', true)
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');

$form->addText('filled3', 'Standard')->setHelperText('Helper Text', true)
    ->setIcon('delete', false)
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');

$form->addGroup('Shaped filled')->setOption('container', Html::el('fieldset', ['class' => 'demo-text-field-shaped']));
$form->addText('shapedFilled1', 'Standard')->setHelperText('Helper Text', true)
    ->setRequired();;
$form->addText('shapedFilled2', 'Standard')->setHelperText('Helper Text', true)
    ->setIcon('event', true)
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');
$form->addText('shapedFilled3', 'Standard')->setHelperText('Helper Text', true)
    ->setIcon('delete', false)
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');

$form->addGroup('Outlined');
$form->addText('outlined1', 'Standard')->setHelperText('Helper Text', true)->setOutline()
    ->setRequired();;
$form->addText('outlined2', 'Standard')->setHelperText('Helper Text', true)->setOutline()
    ->setIcon('event', true)
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');
$form->addText('outlined3', 'Standard')->setHelperText('Helper Text', true)->setOutline()
    ->setIcon('delete', false)
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');

$form->addGroup('Shaped outlined')->setOption('container', Html::el('fieldset', ['class' => 'demo-text-field-outlined-shaped']));
$form->addText('shapedOutlined1', 'Standard')->setHelperText('Helper Text', true)
    ->setOutline()
    ->setRequired();;
$form->addText('shapedOutlined2', 'Standard')->setHelperText('Helper Text', true)
    ->setOutline()
    ->setIcon('event', true)
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');
$form->addText('shapedOutlined3', 'Standard')->setHelperText('Helper Text', true)
    ->setOutline()
    ->setIcon('delete', false)
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');

$form->addGroup('Text Field without label');
$form->addText('noLabel1')->setHelperText('Helper Text', true)->setRequired();
$form->addText('noLabel2')->setHelperText('Helper Text', true)->setOutline()
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');
$form->addText('noLabel3')->setHelperText('Helper Text', true)->setOutline()
    ->setIcon('event')
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');

$form->addGroup('Text Field with character counter');
$form->addText('filledWithCounter1', 'Standard', null, 10)->setRequired()
    ->setHelperText('Helper Text', true);
$form->addText('filledWithCounter2', 'Standard', null, 10)
    ->setHelperText('Helper Text', true)
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');
$form->addText('filledWithCounter3', 'Standard', null, 10)
    ->setHelperText('Helper Text', true)
    ->setIcon('delete', false)
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');

$form->addGroup('Textarea');
$form->addTextArea('textarea', 'Standard')->setHelperText('Helper Text', true)->setRequired();
$form->addTextArea('textarea2', 'Standard')->setHelperText('Helper Text', true)
    ->addRule(\Nette\Forms\Form::NUMERIC, 'Value must be number');

$form->addGroup('Textarea with character counter');
$form->addTextArea('textareaWithCounter', 'Standard', null, null, 144)->setHelperText('Helper Text', true);

$form->addGroup('Full width')->setOption('container', Html::el('fieldset', ['class' => 'group-fullwidth']));
$form->addText('fullwidth', 'Standard')->setHelperText('Helper Text', true)->setFullwidth()->setRequired();

$form->addGroup('Full width textarea')->setOption('container', Html::el('fieldset', ['class' => 'group-fullwidth']));
$form->addTextArea('textareaFullwidth', 'Standard')->setHelperText('Helper Text', true)->setFullwidth()->setRequired();

$form->addGroup(null);
$form->addSubmit('submit', 'Send');

if ($form->isSuccess()) {
    echo '<h2>Form was submitted and successfully validated</h2>';
    Dumper::dump($form->getValues(), [Dumper::COLLAPSE => FALSE]);
    exit;
}
?>
<!DOCTYPE html>
<meta charset="utf-8">
<title>Nette Forms basic example</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Mono">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" media="screen" href="assets/material-components-web.css"/>
<style>
    .demo-content {
        display: -ms-flexbox;
        display: flex;
        justify-content: center;
    }

    .demo-content section {
        width: 900px;
        min-width: 900px;
    }

    .text-field-row {
        display: flex;
        justify-content: space-between;
    }

    .group-specific .text-field-row {
        display: flex;
        justify-content: space-evenly;
    }

    .group-fullwidth .text-field-row {
        display: block;
    }

    .demo-text-field-shaped .mdc-text-field {
        border-radius: 16px 16px 0 0;
    }

    .demo-text-field-outlined-shaped .mdc-text-field .mdc-notched-outline .mdc-notched-outline__leading {
        border-radius: 28px 0 0 28px;
        width: 28px;
    }

    .demo-text-field-outlined-shaped .mdc-text-field .mdc-notched-outline .mdc-notched-outline__notch {
        max-width: calc(100% - 28px * 2);
    }

    .demo-text-field-outlined-shaped .mdc-text-field .mdc-notched-outline .mdc-notched-outline__trailing {
        border-radius: 0 28px 28px 0;
    }

    .mdc-text-field:not(.mdc-text-field--textarea) {
        width: 100%;
    }
</style>
<body class="mdc-typography">

<div class="demo-content">
    <section>
        <h1 class="mdc-typography--headline5">Text Field</h1>
        <p class="mdc-typography--body1">Text fields allow users to input, edit, and select text. Text fields typically
            reside in forms but can appear in other places, like dialog boxes and search.</p>

        <?php echo $form ?>
    </section>
</div>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
<script src="assets/material-components-web.js"></script>
<script src="https://nette.github.io/resources/js/netteForms.js"></script>
<script>
    window.mdc.autoInit();
    //
    $.each(document.querySelectorAll('.mdc-text-field'), function (i, val) {
        mdc.textField.MDCTextField.attachTo(val);
    });
    //
    // document.getElementById('basic-indeterminate-checkbox').indeterminate = true;
    //
    //FORM FIELD
    // var formFields = document.querySelectorAll('.mdc-form-field');
    // for (var i = 0, formField; formField = formFields[i]; i++) {
    // var formFieldInstance = new mdc.formField.MDCFormField(formField);

    // var radio = formField.querySelector('.mdc-radio');
    // if (radio) {
    //     formFieldInstance.input = new mdc.radio.MDCRadio(radio);
    // }
    //
    // var checkboxes = [].slice.call(document.querySelectorAll('.mdc-checkbox'));
    // checkboxes.forEach(function (checkbox) {
    //     var checkboxInstance = new mdc.checkbox.MDCCheckbox(checkbox);
    //
    //     var formField = checkbox.parentElement;
    //     var formFieldInstance = new mdc.formField.MDCFormField(formField);
    //     formFieldInstance.input = checkboxInstance;
    //
    // });
    // }
    //
    // function showErrors(errors, focus) {
    //     errors.forEach(function (error) {
    //         if (error.message) {
    //             var parent = $(error.element).parent();
    //             if (error.element.type === 'checkbox') {
    //                 parent.addClass('mdc-checkbox--invalid');
    //
    //             } else {
    //                 parent.addClass('mdc-text-field--invalid');
    //                 var errorText = parent.next('p.mdc-text-field-helper-text--validation-msg');
    //                 if (errorText.length > 0) {
    //                     errorText.remove();
    //                 }
    //                 $("<p class='mdc-text-field-helptext mdc-text-field-helptext--persistent mdc-text-field-helptext--validation-msg' role='alert'>").text(error.message).insertAfter($(error.element).parent());
    //             }
    //         }
    //         if (focus && error.element.focus) {
    //             error.element.focus();
    //             focus = false;
    //         }
    //     });
    // }
    //
    // function removeErrors(elem) {
    //     if ($(elem).is('form')) {
    //         //skip
    //     } else {
    //         $(elem).parent().next('.mdc-text-field-helper-text--validation-msg').remove();
    //         $(elem).parent().removeClass('mdc-checkbox--invalid');
    //     }
    // }
    //
    // Nette.showFormErrors = function (form, errors) {
    //     removeErrors(form);
    //     showErrors(errors, true);
    // };
    // $(function () {
    //     $(':input').keypress(function () {
    //         removeErrors(this);
    //     });
    //     $(':input').blur(function () {
    //         Nette.formErrors = [];
    //         Nette.validateControl(this);
    //         showErrors(Nette.formErrors);
    //     });
    //     $(':input[type=checkbox]').click(function () {
    //         removeErrors(this);
    //         Nette.formErrors = [];
    //         Nette.validateControl(this);
    //         showErrors(Nette.formErrors);
    //     });
    // });
</script>
</body>