<?php

/**
 * Nette Forms basic example.
 */

declare(strict_types=1);


if (@!include __DIR__ . '/../vendor/autoload.php') {
	die('Install packages using `composer install`');
}

use Nette\Utils\Html;
use Tracy\Debugger;
use Tracy\Dumper;

Debugger::enable();


$form = new \MDCNette\Forms\MDCForm();
$renderer = $form->getRenderer();
$renderer->wrappers['controls']['container'] = 'div class=button-row';

$form->addGroup('Text buttons');
$form->addButton('text1', "Button");
$form->addButton('text2', "Trailing")->setIcon('favorite', false);
$form->addButton('text3', "Leading")->setIcon('event');
$form->addButton('text4', "Dense")->setDense();

$form->addGroup('Raised buttons');
$form->addButton('raised1', "Button")->setRaised();
$form->addButton('raised2', "Trailing")->setIcon('favorite', false)->setRaised();
$form->addButton('raised3', "Leading")->setIcon('event')->setRaised();
$form->addButton('raised4', "Dense")->setRaised()->setDense();

$form->addGroup('Unelevated buttons');
$form->addButton('unelevated1', "Button")->setUnelevated();
$form->addButton('unelevated2', "Trailing")->setIcon('favorite', false)->setUnelevated();
$form->addButton('unelevated3', "Leading")->setIcon('event')->setUnelevated();
$form->addButton('unelevated4', "Dense")->setUnelevated()->setDense();

$form->addGroup('Outlined buttons');
$form->addButton('outlined1', "Button")->setOutline();
$form->addButton('outlined2', "Trailing")->setIcon('favorite', false)->setOutline();
$form->addButton('outlined3', "Leading")->setIcon('event')->setOutline();
$form->addButton('outlined4', "Dense")->setOutline()->setDense();

$form->addGroup('Shaped buttons')->setOption('container', Html::el('fieldset', ['class' => 'demo-button-shaped']));
$form->addButton('shaped1', "Button")->setRaised();
$form->addButton('shaped2', "Trailing")->setRaised()->setIcon('favorite', false);
$form->addButton('shaped3', "Leading")->setRaised()->setIcon('event');
$form->addButton('shaped4', "Dense")->setRaised()->setDense();

if ($form->isSuccess()) {
	echo '<h2>Form was submitted and successfully validated</h2>';
	Dumper::dump($form->getValues(), [Dumper::COLLAPSE => false]);
	exit;
}


?>
<!DOCTYPE html>
<meta charset="utf-8">
<title>Nette Forms basic example</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Mono">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" media="screen" href="assets/material-components-web.css"/>
<style>
    .demo-content {
        display: -ms-flexbox;
        display: flex;
        justify-content: center;
    }

    .demo-content section {
        width: 900px;
        min-width: 900px;
    }

    .button-row {
        display: flex;
        justify-content: space-between;
    }

    .demo-button-shaped .mdc-button {
        border-radius: 16px;
    }

    .mdc-text-field:not(.mdc-text-field--textarea) {
        width: 100%;
    }
</style>
<body class="mdc-typography">

<div class="demo-content">
    <section>
        <h1 class="mdc-typography--headline5">Buttons</h1>
        <p class="mdc-typography--body1">Buttons communicate an action a user can take. They are typically placed
            throughout your UI, in places like dialogs, forms, cards, and toolbars.</p>

		<?php echo $form ?>
    </section>
</div>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
<script src="assets/material-components-web.js"></script>
<script src="https://nette.github.io/resources/js/netteForms.js"></script>
<script>
    mdc.autoInit();
</script>
</body>
