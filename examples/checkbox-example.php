<?php

/**
 * Nette Forms basic example.
 */

declare(strict_types=1);


if (@!include __DIR__ . '/../vendor/autoload.php') {
	die('Install packages using `composer install`');
}

use Nette\Utils\Html;
use Tracy\Debugger;
use Tracy\Dumper;

Debugger::enable();


$form = new \MDCNette\Forms\MDCForm();
$renderer = $form->getRenderer();

$form->addGroup('Checkbox buttons');
$form->addCheckbox('Checkbox', 'Check this out');

$form->addSubmit('send', 'Send')->setIcon('done');

if ($form->isSuccess()) {
	echo '<h2>Form was submitted and successfully validated</h2>';
	Dumper::dump($form->getValues(), [Dumper::COLLAPSE => false]);
	exit;
}


?>
<!DOCTYPE html>
<meta charset="utf-8">
<title>Nette Forms basic example</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Mono">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" media="screen" href="assets/material-components-web.css"/>
<style>
    .demo-content {
        display: -ms-flexbox;
        display: flex;
        justify-content: center;
    }

    .demo-content section {
        width: 900px;
        min-width: 900px;
    }
</style>
<body class="mdc-typography">

<div class="demo-content">
    <section>
        <h1 class="mdc-typography--headline5">Checkbox</h1>
        <p class="mdc-typography--body1">Checkboxes allow the user to select multiple options from a set.</p>

		<?php echo $form ?>
    </section>
</div>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
<script src="assets/material-components-web.js"></script>
<script src="https://nette.github.io/resources/js/netteForms.js"></script>
<script>
    mdc.autoInit();
</script>
</body>
