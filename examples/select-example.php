<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2018 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */


declare(strict_types=1);
if (@!include __DIR__ . '/../vendor/autoload.php') {
    die('Install packages using `composer install`');
}

use MDCNette\Forms\MDCForm;
use MDCNette\Forms\Rendering\MdcRenderer;
use Tracy\Debugger;

Debugger::enable();
$form = new MDCForm();
$form->setRenderer(new MdcRenderer());

$countries = [
    'bu' => 'Buranda',
    'qu' => 'Qumran',
    'st' => 'Saint Georges Island',
    'World' => [
        'bu' => 'Buranda',
        'qu' => 'Qumran',
        'st' => 'Saint Georges Island',
    ],
    '?'     => 'other',
];

$countriesNoGroups = [
    'bu' => 'Buranda',
    'qu' => 'Qumran',
    'st' => 'Saint Georges Island',
    '?'     => 'other',
];

$form->addSelect('country', 'Country:', $countries)
    ->setPrompt('Select your countryy')
    ->setRequired('Select your country')
    ->setDisabled(['bu']);

$form->addSelect('countryFull', 'Country:', $countriesNoGroups)
    ->setFullFeatured()
    ->setRequired('Select your country')
    ->setDisabled(['bu']);

$form->addMultiSelect('multiSelect', 'multi', $countries)->setRequired();

$form->addSubmit('submit', 'Send')->setIcon('favorite');

if ($form->isSuccess()) {
    Debugger::barDump($form->getValues());
}
?>
<!DOCTYPE html>
<meta charset="utf-8">
<title>Nette Forms basic example</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Mono">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" media="screen" href="assets/material-components-web.css"/>

<style>
    .mdc-text-field--textarea .mdc-text-field__input:invalid:not(:focus) {
        border-color: transparent;
    }

    .mdc-form-field .mdc-checkbox--invalid .mdc-checkbox__background {
        border-color: #f44336 !important;
        background-color: rgba(244, 67, 54, .35) !important;
    }

    .mdc-text-field--invalid .mdc-select__surface {
        background-color: rgba(244, 67, 54, .15);
    }

    .mdc-text-field--invalid .mdc-select__surface:focus ~ .mdc-select__bottom-line, .mdc-text-field--invalid .mdc-select__bottom-line, .mdc-text-field--invalid .mdc-select__bottom-line::after {
        background-color: red!important;
    }

    .mdc-select, .mdc-multi-select {
        margin: 16px 0 8px 0;
    }

    p.mdc-text-field-helper-text {
        margin-bottom: 8px;
    }
</style>

<body class="mdc-typography">

<h1>Nette Forms basic example</h1>


<?php echo $form ?>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
<script src="assets/material-components-web.js"></script>
<script src="https://nette.github.io/resources/js/netteForms.js"></script>
<script>
    window.mdc.autoInit();

    // SELECT WITH FULLY FEATURED JS
    $.each(document.querySelectorAll('.mdc-select[role=listbox]'), function (i, val) {
        var select = new mdc.select.MDCSelect(val);
        select.listen('MDCSelect:change', function () {
            var input = $(val).find('input[type=hidden]');
            input.val(select.value);
            removeErrors(input);
            Nette.formErrors = [];
            Nette.validateControl(input);
            showErrors(Nette.formErrors);
        })
    });

    function showErrors(errors, focus) {
        errors.forEach(function (error) {
            if (error.message) {
                var parent = $(error.element).parent();

                var errorText = parent.next('p.mdc-text-field-helper-text--validation-msg');
                if (errorText.length > 0) {
                    errorText.remove();
                }

                if (error.element.type === 'checkbox') {
                    parent.addClass('mdc-checkbox--invalid');

                } else if (error.element.type === 'select-one') {
                    parent.addClass('mdc-text-field--invalid');

                    $("<p class='mdc-text-field-helper-text mdc-text-field-helper-text--persistent mdc-text-field-helper-text--validation-msg' " +
                        "id='pw-validation-msg' role='alert'>")
                        .text(error.message).insertAfter($(error.element).parent());
                } else {
                    parent.addClass('mdc-text-field--invalid');

                    $("<p class='mdc-text-field-helper-text mdc-text-field-helper-text--persistent mdc-text-field-helper-text--validation-msg' " +
                        "id='pw-validation-msg' role='alert'>")
                        .text(error.message).insertAfter($(error.element).parent());

                }

            }

            if (focus && error.element.focus) {
                if ($(error.element).attr('mdcselect') !== undefined) {
                    $(error.element).parent().find('.mdc-select__surface').focus();
                } else {
                    error.element.focus();
                }
                focus = false;
            }
        });
    }

    function removeErrors(elem) {
        if ($(elem).is('form')) {
            //skip
        } else {
            // console.log($(elem).parent());
            $(elem).parent().next('.mdc-text-field-helper-text--validation-msg').remove();
            $(elem).parent().removeClass('mdc-text-field--invalid');
            $(elem).parent().removeClass('mdc-checkbox--invalid');
        }
    }

    Nette.showFormErrors = function (form, errors) {
        removeErrors(form);
        showErrors(errors, true);
    };
    $(function () {
        $(':input').keypress(function () {
            removeErrors(this);
        });
        $(':input').blur(function () {
            removeErrors(this);
            Nette.formErrors = [];
            Nette.validateControl(this);
            showErrors(Nette.formErrors);
        });
        $('select').change(function () {
            removeErrors(this);
            Nette.formErrors = [];
            Nette.validateControl(this);
            showErrors(Nette.formErrors);
        });
        $(':input[type=checkbox]').click(function () {
            removeErrors(this);
            Nette.formErrors = [];
            Nette.validateControl(this);
            showErrors(Nette.formErrors);
        });
    });
</script>
</body>

