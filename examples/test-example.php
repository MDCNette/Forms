<?php

/**
 * Nette Forms basic example.
 */

declare(strict_types=1);


if (@!include __DIR__ . '/../vendor/autoload.php') {
	die('Install packages using `composer install`');
}

use Nette\Utils\Html;
use Tracy\Debugger;
use Tracy\Dumper;

Debugger::enable();

$form = new \MDCNette\Forms\MDCForm();
//$form = new \Nette\Forms\Form();

$form->addGroup('test');
$form->addRadioList('multiRadio', 'TEST', ['heze' => 'aaa', 'asdsdf' => 'bbb'])->setOption('id', 'rrra');
$form->addGroup('trest');
$form->addCheckboxList('multiCheckbox', 'Testovací okenko', ['heze' => 'aaa', 'asdsdf' => 'bbb'])->setOption('id', 'oiiii');
$form->addGroup('just');
$form->addCheckbox('oioi', 'hou')->setOption('id', 'trest');
$form->addGroup('test');
$form->addSelect('check', 'hou', ['heze' => 'aaa', 'asdsdf' => 'bbb'])->setOutlined();


$form->addSubmit('ok', 'ok');


if ($form->isSuccess()) {
	echo '<h2>Form was submitted and successfully validated</h2>';
	Dumper::dump($form->getValues(), [Dumper::COLLAPSE => false]);
	exit;
}


?>
<!DOCTYPE html>
<meta charset="utf-8">
<title>Nette Forms basic example</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Mono">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" media="screen" href="assets/material-components-web.css"/>

<style>
    .text-field-container {
        margin-bottom: 16px;
    }
</style>

<h1>Nette Forms basic example</h1>

<?php echo $form ?>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
<script src="assets/material-components-web.js"></script>
<script src="https://nette.github.io/resources/js/netteForms.js"></script>
<script>
    window.mdc.autoInit();
</script>
