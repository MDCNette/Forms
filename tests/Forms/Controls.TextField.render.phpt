<?php
/**
 * Test: MDCNette\Forms\Components\TextInput
 */
declare(strict_types=1);

use MDCNette\Forms\Controls\TextInput;
use Mdcnette\Forms\MdcForm;
use Tester\Assert;

require __DIR__ . '/../Bootstrap.php';

$form = new MdcForm();
$input = $form->addText('text', 'Label')->setValue('value');

Assert::type(TextInput::class, $input);