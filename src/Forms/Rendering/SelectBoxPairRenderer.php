<?php

declare(strict_types=1);


namespace MDCNette\Forms\Rendering;


use Nette\Forms\IControl;
use Nette\Utils\Html;

class SelectBoxPairRenderer extends PairRenderer {

	public function renderPair(IControl $control): Html {
		$pair = parent::renderPair($control);

		$pair->addHtml($this->renderControl($control));

		return $pair;
	}

}