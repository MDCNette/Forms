<?php

declare(strict_types=1);


namespace MDCNette\Forms\Rendering;


use MDCNette\Forms\MDCForm;
use MDCNette\Forms\Rendering\PairRenderer;
use Nette\Forms\IControl;
use Nette\Utils\Html;

class CheckboxPairRenderer extends PairRenderer {


	private $checkboxWrappers = [
		'field' => 'div class=mdc-form-field',
		'container' => 'div class=mdc-checkbox',
		'background' => 'div class=mdc-checkbox__background',
		'mixedmark' => 'div class=mdc-checkbox__mixedmark',
		'.checkmark' => 'mdc-checkbox__checkmark',
		'.path' => 'mdc-checkbox__checkmark-path',
		'.disabled' => 'mdc-checkbox--disabled',
	];

	public function __construct(MDCForm $form, int $counter, array $wrappers) {
		parent::__construct($form, $counter, $wrappers);
		$this->wrappers['checkbox'] = $this->checkboxWrappers;
		$this->wrappers['pair']['container'] = 'div class=checkbox-container';
	}

	public function renderPair(IControl $control): Html {
		$pair = parent::renderPair($control);

		$path = Html::el('path', [
			'class' => $this->getValue('checkbox .path'),
			'fill' => 'none',
			'd' => 'M1.73,12.91 8.1,19.28 22.79,4.59']);
		$svg = Html::el('svg', [
			'class' => $this->getValue('checkbox .checkmark'),
			'viewBox' => '0 0 24 24']);
		$mixedMark = $this->getWrapper('checkbox mixedmark');
		$background = $this->getWrapper('checkbox background');
		$background->addHtml($svg->addHtml($path));
		$background->addHtml($mixedMark);

		$container = $this->getWrapper('checkbox container');
		$container->class($control->disabled ? $this->getValue('checkbox .disabled') : '', true);
		$container->addAttributes(['data-mdc-auto-init' => 'MDCCheckbox']);
		$container->addHtml($this->renderControl($control));
		$container->addHtml($background);


		$field = $this->getWrapper('checkbox field');
		$field->addAttributes(['data-mdc-auto-init' => 'MDCFormField']);
		$field->addHtml($container);
		$label = Html::el('label', ['for' => $control->getHtmlId()])->addText($control->getCaption());
		$field->addHtml($label);

		$pair->addHtml($field);

		return $pair;
	}

}