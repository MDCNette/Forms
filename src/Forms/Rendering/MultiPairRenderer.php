<?php

declare(strict_types=1);


namespace MDCNette\Forms\Rendering;


use MDCNette\Forms\MDCControls\Button;
use MDCNette\Forms\MDCControls\SubmitButton;
use MDCNette\Forms\MDCForm;
use Nette\Utils\Html;

class MultiPairRenderer extends PairRenderer {

	protected $buttonWrappers = [
		'container' => 'button class=mdc-button',
		'label' => 'span class=mdc-button__label',
		'.outline' => 'mdc-button--outlined',
		'.unelevated' => 'mdc-button--unelevated',
		'.raised' => 'mdc-button--raised',
		'.dense' => 'mdc-button--dense',
	];

	public function __construct(MDCForm $form, int $counter, array $wrappers) {
		parent::__construct($form, $counter, $wrappers);

		$this->wrappers['button'] = $this->buttonWrappers;
	}

	public function renderPairs(array $controls): Html {
		$s = [];
		foreach ($controls as $control) {
			if (!$control instanceof \Nette\Forms\IControl) {
				throw new \Nette\InvalidArgumentException('Argument must be array of Nette\Forms\IControl instances.');
			}

			$el = $this->renderControl($control);

			if ($control instanceof Button || $control instanceof SubmitButton) {
				$icon = $control->getIcon();

				if ($icon != null) {
					if ($control->icon->isLeading()) {
						$el->addHtml($icon);
					}
					$el->addHtml($this->getWrapper('button label')->addText($control->getCaption()));
					if (!$control->icon->isLeading()) {
						$el->addHtml($icon);
					}
				} else {
					$el->addHtml($this->getWrapper('button label')->addText($control->getCaption()));
				}

				if ($control->getOption('outline', false)) {
					$el->class($this->getValue('button .outline'), true);
				} else if ($control->getOption('unelevated', false)) {
					$el->class($this->getValue('button .unelevated'), true);
				} else if ($control->getOption('raised', false)) {
					$el->class($this->getValue('button .raised'), true);
				}

				if ($control->getOption('dense', false)) {
					$el->class($this->getValue('button .dense'), true);
				}
			}

			$s[] = $el;
		}

		$pair = $this->getWrapper('pair container');
		$pair->addHtml($this->renderLabel($control));
		$pair->addHtml($this->getWrapper('control container')->setHtml(implode(' ', $s)));

		return $pair;
	}

	/**
	 * Renders 'control' part of visual row of controls.
	 */
	public function renderControl(\Nette\Forms\IControl $control): Html {
		$control->setOption('rendered', true);
		$el = $control->getControl();
		if ($el instanceof Html) {
			if ($el->getName() === 'input') {
				$el->class($this->getValue("control .$el->type"), true);
			}

			$el->class($this->getValue('control .error'), $control->hasErrors());
		}

		return $el;
	}
}