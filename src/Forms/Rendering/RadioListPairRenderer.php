<?php

declare(strict_types=1);


namespace MDCNette\Forms\Rendering;


use MDCNette\Forms\MDCForm;
use MDCNette\Forms\Rendering\PairRenderer;
use Nette\Forms\IControl;
use Nette\Utils\Html;

class RadioListPairRenderer extends PairRenderer {

	public function __construct(MDCForm $form, int $counter, array $wrappers) {
		parent::__construct($form, $counter, $wrappers);
		$this->wrappers['pair']['container'] = 'div class=radiolist-container';
	}


	public function renderPair(IControl $control): Html {

		$pair = parent::renderPair($control);
		$pair->addHtml($this->renderLabel($control));
		$pair->addHtml($this->renderControl($control));

		return $pair;
	}

}