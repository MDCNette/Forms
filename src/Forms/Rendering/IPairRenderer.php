<?php

declare(strict_types=1);


namespace MDCNette\Forms\Rendering;


use Nette\Forms\IControl;
use Nette\Utils\Html;

interface IPairRenderer
{

    public function renderPair(IControl $control): Html;

}