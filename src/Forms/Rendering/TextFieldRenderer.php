<?php

declare(strict_types=1);


namespace MDCNette\Forms\Rendering;


use MDCNette\Forms\MDCControls\TextArea;
use MDCNette\Forms\MDCControls\TextBase;
use MDCNette\Forms\MDCControls\TextInput;
use MDCNette\Forms\MDCForm;
use MDCNette\Forms\Parts\CharacterCounter;
use MDCNette\Forms\Parts\Outline;
use Nette\Forms\IControl;
use Nette\Utils\Html;
use Nette\Utils\IHtmlString;

class TextFieldRenderer extends PairRenderer
{

    protected $textfieldWrappers = [
        'container' => 'div class=mdc-text-field data-mdc-auto-init=MDCTextField',
        'lineripple' => 'div class=mdc-line-ripple',
        '.fullwidth' => 'mdc-text-field--fullwidth',
        '.outline' => 'mdc-text-field--outlined',
        '.nolabel' => 'mdc-text-field--no-label',
        '.textarea' => 'mdc-text-field--textarea',
        '.leading-icon' => 'mdc-text-field--with-leading-icon',
        '.trailing-icon' => 'mdc-text-field--with-trailing-icon',
        '.error' => 'mdc-text-field--invalid'
    ];

    /**
     * TextFieldRenderer constructor.
     */
	public function __construct(MDCForm $form, int $counter, array $wrappers) {
		parent::__construct($form, $counter, $wrappers);
        $this->wrappers['textfield'] = $this->textfieldWrappers;
        $this->wrappers['pair']['container'] = 'div class=text-field-container';
    }

    public function renderPair(IControl $control): Html
    {
        $pair = parent::renderPair($control);

        $textfield = $this->getWrapper('textfield container');
        $textfield->class($control->hasErrors() ? $this->getValue('textfield .error') : null, true);

        if ($control instanceof TextArea) {
            if ($control->getControl()->maxlength != null) {
                $textfield->addHtml(new CharacterCounter());
            }
            $textfield->addHtml($this->renderControl($control));
        }

        if ($control instanceof TextInput) {
            $icon = $control->getIcon();
            if ($icon != null) {
                $textfield->class($icon->isLeading() ?
                    $this->getValue('textfield .leading-icon') :
                    $this->getValue('textfield .trailing-icon'), true);
                $textfield->addHtml($icon->isLeading() ? $icon : null);
                $textfield->addHtml($this->renderControl($control));
                $textfield->addHtml(!$icon->isLeading() ? $icon : null);
            } else {
                $textfield->addHtml($this->renderControl($control));
            }
        }

        $label = $this->renderLabel($control);

        if ($control->getOption('type') == 'textarea') {
            $textfield->class($this->getValue('textfield .textarea'), true);
            $textfield->addHtml(new Outline($label));
        }

        if ($control->getOption('fullwidth') != null) { // FULLWIDTH
            $textfield->class($this->getValue('textfield .fullwidth'), true);
            $textfield->class($this->getValue('textfield .nolabel'), true);
        } else {
            $textfield->class($label === null ? $this->getValue('textfield .nolabel') : null, true);

            if ($control->getOption('type') == 'text') {
                if ($control->getOption('outline') != null) { // OUTLINE
                    $textfield->class($this->getValue('textfield .outline'), true);
                    $textfield->addHtml(new Outline($label));
                } else { // ELSE
                    $textfield->addHtml($label);
                    $textfield->addHtml(Html::el($this->getValue('textfield lineripple')));
                }
            }
        }

        $pair->addHtml($textfield);
        if (($helperText = $control->getHelperText()) != null) {
            $pair->addHtml($helperText);
        }

        return $pair;
    }

    /**
     * Renders validation errors (per control).
     */
    public function renderErrors(\Nette\Forms\IControl $control): string
    {
        $errors = $control->getErrors();
        if (!$errors) {
            return '';
        }

        if ($control instanceof TextBase) {
            $control->setHelperText($errors[0], true, true);
        }

        return '';
    }
}