<?php

declare(strict_types=1);


namespace MDCNette\Forms\Rendering;


use MDCNette\Forms\MDCForm;
use Nette\Forms\IControl;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Nette\Utils\IHtmlString;

abstract class PairRenderer implements IPairRenderer {
	use \Nette\SmartObject;

	/** @var MDCForm */
	protected $form;

	/** @var int */
	protected $counter;

	public $wrappers = [
		'pair' => [
			'container' => null,
			'.required' => null,
			'.disabled' => null,
			'.optional' => null,
			'.odd' => null,
			'.error' => null,
		],

		'control' => [
			'container' => null,
			'.odd' => null,

			'requiredsuffix' => '',
			'errorcontainer' => 'span class=error',
			'erroritem' => '',

			'.required' => null,
			'.error' => null,
			'.submit' => 'button',
			'.image' => 'imagebutton',
			'.button' => 'button',
		],

		'label' => [
			'container' => null,
			'suffix' => null,
			'requiredsuffix' => '',
		],
	];

	public function __construct(MDCForm $form, int $counter, array $wrappers) {
		$this->counter = $counter;
		$this->form = $form;
		$this->wrappers = Arrays::mergeTree($wrappers, $this->wrappers);
	}


	public function renderPair(IControl $control): Html {
		$pair = $this->getWrapper('pair container');

		$pair->class($control->isDisabled() ? $this->getValue('pair .disabled') : null, true);
		$pair->class($this->getValue($control->isRequired() ? 'pair .required' : 'pair .optional'), true);
		$pair->class($control->hasErrors() ? $this->getValue('pair .error') : null, true);
		$pair->class($control->getOption('class'), true);
		if (++$this->counter % 2) {
			$pair->class($this->getValue('pair .odd'), true);
		}
		$pair->id = $control->getOption('id');

		return $pair;
	}

	/**
	 * Renders 'control' part of visual row of controls.
	 */
	public function renderControl(\Nette\Forms\IControl $control): Html {
		$body = $this->getWrapper('control container');
		if ($this->counter % 2) {
			$body->class($this->getValue('control .odd'), true);
		}

		$control->setOption('rendered', true);
		$el = $control->getControl();
		if ($el instanceof Html) {
			$el->class($this->getValue('control .error'), $control->hasErrors());
		}
		return $body->setHtml($el . $this->renderErrors($control));
	}

	/**
	 * Renders 'label' part of visual row of controls.
	 */
	public function renderLabel(\Nette\Forms\IControl $control): ?Html {
		$suffix =
			$this->getValue('label suffix') .
			($control->isRequired() ? $this->getValue('label requiredsuffix') : '');
		$label = $control->getLabel();
		if ($label instanceof Html) {
			$label->addHtml($suffix);
			$label->class($control->isRequired() ? $this->getValue('control .required') : null, true);
		} elseif ($label != null) { // @intentionally ==
			$label .= $suffix;
		} else {
			return null;
		}

		return $this->getWrapper('label container')->setHtml((string)$label);
	}

	/**
	 * Renders validation errors (per control).
	 */
	public function renderErrors(\Nette\Forms\IControl $control): string {
		$errors = $control->getErrors();
		if (!$errors) {
			return '';
		}

		$container = $this->getWrapper('control errorcontainer');
		$item = $this->getWrapper('control erroritem');

		foreach ($errors as $error) {
			$item = clone $item;
			if ($error instanceof IHtmlString) {
				$item->addHtml($error);
			} else {
				$item->setText($error);
			}
			$container->addHtml($item);
		}
		return "\n" . $container->render(1);
	}


	protected function getWrapper(string $name): Html {
		$data = $this->getValue($name);
		return $data instanceof Html ? clone $data : Html::el($data);
	}


	/**
	 * @return mixed
	 */
	protected function getValue(string $name) {
		$name = explode(' ', $name);
		$data = &$this->wrappers[$name[0]][$name[1]];
		return $data;
	}
}