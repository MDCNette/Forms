<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Forms\Controls;

use Nette\Forms\Controls\SubmitButton;

/**
 * Submittable image button form control.
 */
class ImageButton extends SubmitButton
{

    /**
     * @param  string  $src  URI of the image
     * @param  string  $alt  alternate text for the image
     */
    public function __construct(string $src = null, string $alt = null)
    {
        parent::__construct();
        $this->control->type = 'image';
        $this->control->src = $src;
        $this->control->alt = $alt;
    }


    /**
     * Loads HTTP data.
     */
    public function loadHttpData(): void
    {
        parent::loadHttpData();
        $this->value = $this->value
            ? [(int) array_shift($this->value), (int) array_shift($this->value)]
            : null;
    }


    /**
     * Returns HTML name of control.
     */
    public function getHtmlName(): string
    {
        return parent::getHtmlName() . '[]';
    }
}
