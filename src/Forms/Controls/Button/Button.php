<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Forms\Controls;

use Nette\Utils\Html;


/**
 * @author Tomáš Němec <tms.nemec@gmail.com>
 *
 * @property-read Html $icon
 */
class Button extends BaseControl {

    /**
     * @param  string|object  $caption
     */
    public function __construct($caption = null)
    {
        parent::__construct($caption);
        $this->control->type = 'button';
        $this->setOption('type', 'button');
    }


    /**
     * Is button pressed?
     */
    public function isFilled(): bool
    {
        $value = $this->getValue();
        return $value !== null && $value !== [];
    }


    /**
     * Bypasses label generation.
     */
    public function getLabel($caption = null): void
    {
    }


    /**
     * Generates control's HTML element.
     * @param  string|object  $caption
     */
    public function getControl($caption = null): \Nette\Utils\Html
    {
        $this->setOption('rendered', true);
        $el = clone $this->control;
        return $el->addAttributes([
            'name' => $this->getHtmlName(),
            'disabled' => $this->isDisabled(),
            'value' => $this->translate($caption === null ? $this->getCaption() : $caption),
        ]);
    }
}