<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

namespace MDCNette\Forms\Controls;

use MDCNette\Forms\MDCContainer;
use MDCNette\Forms\MDCForm;
use Nette\Forms\ISubmitterControl;


/**
 * @author Tomáš Němec <tms.nemec@gmail.com>
 */
class SubmitButton extends Button implements ISubmitterControl {

    /** @var callable[]  function (SubmitButton $sender): void; Occurs when the button is clicked and form is successfully validated */
    public $onClick;

    /** @var callable[]  function (SubmitButton $sender): void; Occurs when the button is clicked and form is not validated */
    public $onInvalidClick;

    /** @var array|null */
    private $validationScope;


    /**
     * @param  string|object  $caption
     */
    public function __construct($caption = null)
    {
        parent::__construct($caption);
        $this->control->type = 'submit';
        $this->setOmitted(true);
    }


    /**
     * Loads HTTP data.
     */
    public function loadHttpData(): void
    {
        parent::loadHttpData();
        if ($this->isFilled()) {
            $this->getForm()->setSubmittedBy($this);
        }
    }


    /**
     * Tells if the form was submitted by this button.
     */
    public function isSubmittedBy(): bool
    {
        return $this->getForm()->isSubmitted() === $this;
    }


    /**
     * Sets the validation scope. Clicking the button validates only the controls within the specified scope.
     * @return SubmitButton
     */
    public function setValidationScope(?iterable $scope)
    {
        if ($scope === null) {
            $this->validationScope = null;
        } else {
            $this->validationScope = [];
            foreach ($scope ?: [] as $control) {
                if (!$control instanceof MDCContainer && !$control instanceof \Nette\Forms\IControl) {
                    throw new \Nette\InvalidArgumentException('Validation scope accepts only Nette\Forms\Container or Nette\Forms\IControl instances.');
                }
                $this->validationScope[] = $control;
            }
        }
        return $this;
    }


    /**
     * Gets the validation scope.
     */
    public function getValidationScope(): ?array
    {
        return $this->validationScope;
    }


    /**
     * Fires click event.
     */
    public function click(): void
    {
        $this->onClick($this);
    }


    /**
     * Generates control's HTML element.
     * @param  string|object  $caption
     */
    public function getControl($caption = null): \Nette\Utils\Html
    {
        $scope = [];
        foreach ((array) $this->validationScope as $control) {
            $scope[] = $control->lookupPath(MDCForm::class);
        }
        return parent::getControl($caption)->addAttributes([
            'formnovalidate' => $this->validationScope !== null,
            'data-nette-validation-scope' => $scope ?: null,
        ]);
    }
}