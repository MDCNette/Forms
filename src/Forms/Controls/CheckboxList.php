<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

namespace MDCNette\Forms\Controls;


use Nette\Utils\Html;

class CheckboxList extends \MDCNette\Forms\Controls\MultiChoiceControl {
    /** @var Html  separator element template */
    protected $separator;

    /** @var Html  container element template */
    protected $container;

    /** @var Html  item label template */
    protected $itemLabel;


    /**
     * @param  string|object  $label
     */
    public function __construct($label = null, array $items = null)
    {
        parent::__construct($label, $items);
        $this->control->type = 'checkbox';
        $this->container = Html::el();
        $this->separator = Html::el('br');
        $this->itemLabel = Html::el('label');
        $this->setOption('type', 'checkbox');
    }



    /**
     * Generates label's HTML element.
     * @param  string|object  $caption
     */
    public function getLabel($caption = null): Html
    {
        return parent::getLabel($caption)->for(null);
    }


    public function getControlPart($key = null): Html
    {
        $key = key([(string) $key => null]);
        return parent::getControl()->addAttributes([
            'id' => $this->getHtmlId() . '-' . $key,
            'checked' => in_array($key, (array) $this->value, true),
            'disabled' => is_array($this->disabled) ? isset($this->disabled[$key]) : $this->disabled,
            'required' => null,
            'value' => $key,
        ]);
    }


    public function getLabelPart($key = null): Html
    {
        $itemLabel = clone $this->itemLabel;
        return func_num_args()
            ? $itemLabel->setText($this->translate($this->items[$key]))->for($this->getHtmlId() . '-' . $key)
            : $this->getLabel();
    }


    /**
     * Returns separator HTML element template.
     */
    public function getSeparatorPrototype(): Html
    {
        return $this->separator;
    }


    /**
     * Returns container HTML element template.
     */
    public function getContainerPrototype(): Html
    {
        return $this->container;
    }


    /**
     * Returns item label HTML element template.
     */
    public function getItemLabelPrototype(): Html
    {
        return $this->itemLabel;
    }
}