<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Forms\Parts;

use Nette\Utils\Html;

class HelperText extends Html
{

    const HELPER_LINE = 'mdc-text-field-helper-line';
    const HELPER_TEXT = 'mdc-text-field-helper-text';
    const HELPER_TEXT_PERSISTENT = 'mdc-text-field-helper-text--persistent';
    const HELPER_TEXT_VALIDATION = 'mdc-text-field-helper-text--validation-msg';

    /**
     * HelpText constructor.
     *
     * @param string $message
     * @param string $htmlId
     */
    public function __construct(?string $message, bool $persistent = false,  bool $validation = false, bool $counter = false)
    {
        $helperWrapper = Html::el('div', ['class' => self::HELPER_LINE]);
        {
            $helperText = Html::el('p', ['class' => self::HELPER_TEXT]);
            $helperText->class($persistent ? self::HELPER_TEXT_PERSISTENT : null, true);
            $helperText->class($validation ? self::HELPER_TEXT_VALIDATION : null, true);
            $helperText->setText($message);
        }
        $helperWrapper->addHtml($helperText);
        $helperWrapper->addHtml($counter ? new CharacterCounter() : null);
        $this->setHtml($helperWrapper);
    }
}