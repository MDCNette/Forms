<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Forms\parts;


/**
 * Class Icon
 * @package MDCNette\Forms\Components\TextInput
 *
 * @property-read bool $leading
 */
class ButtonIcon extends Icon {
	protected $class = 'material-icons mdc-button__icon';
}