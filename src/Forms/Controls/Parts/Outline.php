<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Forms\Parts;

use Nette\Utils\Html;


/**
 * Class Outline
 * @package MDCNette\Forms\Components\TextInput
 *
 */
class Outline extends Html
{

    const OUTLINE = 'mdc-notched-outline';
    const OUTLINE_NOTCH = 'mdc-notched-outline__notch';
    const OUTLINE_LEADING = 'mdc-notched-outline__leading';
    const OUTLINE_TRAILING = 'mdc-notched-outline__trailing';

    /**
     * Icon constructor.
     *
     * @param string $name
     * @param bool $leading
     * @param bool $clickable
     */
    public function __construct(Html $label = null)
    {
        $outline = Html::el('div', ['class' => self::OUTLINE]);
        $outline->addHtml(Html::el('div', ['class' => self::OUTLINE_LEADING]));
        if ($label != null) {
            $notch = Html::el('div', ['class' => self::OUTLINE_NOTCH]);
            $notch->addHtml($label);
            $outline->addHtml($notch);
        }
        $outline->addHtml(Html::el('div', ['class' => self::OUTLINE_TRAILING]));
        $this->setHtml($outline);
    }
}