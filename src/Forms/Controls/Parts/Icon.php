<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Forms\parts;

use Nette\Utils\Html;


/**
 * Class Icon
 * @package MDCNette\Forms\Components\TextInput
 *
 * @property-read bool $leading
 */
class Icon extends Html
{

    protected $class = 'material-icons';

    /** @var  bool */
    private $leading;

    /**
     * Icon constructor.
     *
     * @param string $name
     * @param bool $leading
     * @param bool $clickable
     */
    public function __construct(string $name, bool $leading)
    {
        $this->leading = $leading;

        $icon = Html::el(
            'i', ['class' => $this->class])
            ->addText($name);

        $this->setHtml($icon);
    }

    /**
     * @return bool
     */
    public function isLeading(): bool
    {
        return $this->leading;
    }
}