<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Forms\Parts;

use Nette\Utils\Html;


/**
 * Class Character Counter
 * @package MDCNette\Forms\Components\TextInput
 */
class CharacterCounter extends Html
{

    const CHARACTER_COUNTER = 'mdc-text-field-character-counter';

    /**
     * CharacterCounter constructor.
     */
    public function __construct()
    {
        $this->setHtml(Html::el('div', ['class' => self::CHARACTER_COUNTER]));
    }
}