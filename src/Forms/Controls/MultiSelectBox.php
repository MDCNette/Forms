<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */


namespace MDCNette\Forms\Controls;


use MDCNette\Forms\Helpers;
use Nette\Forms\Controls\MultiChoiceControl;
use Nette\Utils\Arrays;
use Nette\Utils\Html;

class MultiSelectBox extends MultiChoiceControl {

    /** @var array of option / optgroup */
    private $options = [];

    /** @var array */
    private $optionAttributes = [];


    public function __construct($label = NULL, array $items = NULL) {
        parent::__construct($label, $items);
        $this->label = NULL;
        $this->control->class('mdc-multi-select mdc-list', TRUE);
        $this->setOption('type', 'select');
    }


    /**
     * Sets options and option groups from which to choose.
     *
     * @param array $items
     * @param bool  $useKeys
     *
     * @return MultiChoiceControl
     */
    public function setItems(array $items, $useKeys = TRUE): MultiChoiceControl {
        if (!$useKeys) {
            $res = [];
            foreach ($items as $key => $value) {
                unset($items[ $key ]);
                if (\is_array($value)) {
                    foreach ($value as $val) {
                        $res[ $key ][ (string)$val ] = $val;
                    }
                } else {
                    $res[ (string)$value ] = $value;
                }
            }
            $items = $res;
        }
        $this->options = $items;

        return parent::setItems(Arrays::flatten($items, TRUE));
    }


    /**
     * Generates control's HTML element.
     * @return Html
     */
    public function getControl(): Html {
        $items = [];
        foreach ($this->options as $key => $value) {
            $items[ \is_array($value) ? $this->translate($key) : $key ] = $this->translate($value);
        }

        return Helpers::createSelectBox(
            $items,
            [
                'disabled:' => \is_array($this->disabled) ? $this->disabled : NULL,
                'class' => 'mdc-list-item'
            ] + $this->optionAttributes,
            $this->value
        )->addAttributes(parent::getControl()->attrs)->multiple(TRUE);
    }


    /**
     * @param array $attributes
     *
     * @return self
     */
    public function addOptionAttributes(array $attributes): self {
        $this->optionAttributes = $attributes + $this->optionAttributes;

        return $this;
    }

    /**
     * Generates label's HTML element.
     *
     * @param  string|object
     *
     * @return Html|string
     */
    public function getLabel($caption = NULL) {
        if ($this->label) {
            /** @var Html $label */
            $label = clone $this->label;
            if ($label) {
                $label->for = $this->getHtmlId();
                $label->setText($this->translate($caption ?? $this->caption));
                return $label;
            }

            return NULL;
        }

        return NULL;
    }
}