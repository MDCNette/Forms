<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Forms\Controls;

/**
 * Class TextArea
 * @package MDCNette\Forms\Components\TextArea
 */
class TextArea extends \MDCNette\Forms\MDCControls\TextBase
{
    /**
     * @param  string|object  $label
     */
    public function __construct($label = null, int $maxLength = null)
    {
        parent::__construct($label, $maxLength);
        $this->control->setName('textarea');
        $this->setOption('type', 'textarea');
    }


    /**
     * Generates control's HTML element.
     */
    public function getControl(): \Nette\Utils\Html
    {
        return parent::getControl()
            ->setText((string) $this->getRenderedValue());
    }
}