<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;


use MDCNette\Forms\parts\Icon;

interface IIcon
{

    /**
     * @param string $name name of material icon
     * @param bool $leading flag for position of the icon
     * @return $this
     */
    public function setIcon(string $name, bool $leading = true);

    public function getIcon(): ?Icon;
}