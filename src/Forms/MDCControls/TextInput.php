<?php


namespace MDCNette\Forms\MDCControls;


use MDCNette\Forms\parts\Icon;
use MDCNette\Forms\parts\TextFieldIcon;

class TextInput extends \MDCNette\Forms\Controls\TextInput implements IIcon
{

    /** @var Icon */
    protected $icon;

    /**
     * TextInput constructor.
     */
    public function __construct($label = null, int $maxLength = null)
    {
        parent::__construct($label, $maxLength);
        $this->control->class('mdc-text-field__input');
        $this->label->class('mdc-floating-label');
    }

    public function setFullwidth()
    {
        if ($this->caption != null) {
            $this->control->addAttributes(['placeholder' => $this->caption, 'aria-label' => $this->caption]);
        }
        return parent::setFullwidth();
    }


    /**
     * @param string $name
     * @param bool $leading
     * @return $this
     */
    public function setIcon(string $name, bool $leading = true)
    {
        $this->icon = new TextFieldIcon($name, $leading);
        return $this;
    }

    public function getIcon(): ?Icon
    {
        return $this->icon;

    }
}