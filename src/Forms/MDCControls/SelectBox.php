<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;


use MDCNette\Forms\Helpers;
use Nette\Utils\Html;

class SelectBox extends \MDCNette\Forms\Controls\SelectBox implements ISelectBox {

	/** @var array of option / optgroup */
	private $options = [];

	/** @var mixed */
	private $prompt = false;

	/** @var array */
	private $optionAttributes = [];

	private $outlined = false;

	/**
	 * Sets first prompt item in select box.
	 * @param string|object $prompt
	 * @return SelectBox
	 */
	public function setPrompt($prompt) {
		$this->prompt = $prompt;
		return $this;
	}


	/**
	 * Returns first prompt item?
	 * @return mixed
	 */
	public function getPrompt() {
		return $this->prompt;
	}

	/**
	 * Sets options and option groups from which to choose.
	 * @return SelectBox
	 */
	public function setItems(array $items, bool $useKeys = true) {
		if (!$useKeys) {
			$res = [];
			foreach ($items as $key => $value) {
				unset($items[$key]);
				if (is_array($value)) {
					foreach ($value as $val) {
						$res[$key][(string)$val] = $val;
					}
				} else {
					$res[(string)$value] = $value;
				}
			}
			$items = $res;
		}
		$this->options = $items;
		return parent::setItems(\Nette\Utils\Arrays::flatten($items, true));
	}

	/**
	 * Generates control's HTML element.
	 */
	public function getControl(): \Nette\Utils\Html {
		$items = $this->prompt === false ? [] : ['' => $this->translate($this->prompt)];
		foreach ($this->options as $key => $value) {
			$items[is_array($value) ? $this->translate($key) : $key] = $this->translate($value);
		}

		$container = Html::el('div', ['class' => 'mdc-select', 'data-mdc-auto-init' => 'MDCSelect']);
		$container->class($this->outlined ? 'mdc-select--outlined' : '', true);
		$container->addHtml(Html::el('i', ['class' => 'mdc-select__dropdown-icon']));
		$container->addHtml(
			Helpers::createSelectBox(
				$items,
				[
					'disabled:' => is_array($this->disabled) ? $this->disabled : null,
				] + $this->optionAttributes,
				$this->value
			)->addAttributes(parent::getControl()->attrs));
		$label = Html::el('label', ['class' => 'mdc-floating-label'])->setText($this->caption);
		if ($this->outlined) {
			$container->addHtml(
				Html::el('div', ['class' => 'mdc-notched-outline'])
					->addHtml(Html::el('div', ['class' => 'mdc-notched-outline__leading']))
					->addHtml(Html::el('div', ['class' => 'mdc-notched-outline__notch'])->addHtml($label))
					->addHtml(Html::el('div', ['class' => 'mdc-notched-outline__trailing']))
			);
		} else {
			$container->addHtml($label);
		}
		$container->addHtml(Html::el('div', ['class' => 'mdc-line-ripple']));


		return $container;
	}

	/**
	 * @return \MDCNette\Forms\Controls\SelectBox
	 */
	public function addOptionAttributes(array $attributes) {
		$this->optionAttributes = $attributes + $this->optionAttributes;
		return $this;
	}

	public function isOk(): bool {
		return $this->isDisabled()
			|| $this->prompt !== false
			|| $this->getValue() !== null
			|| !$this->options
			|| $this->control->size > 1;
	}

	public function getOptionAttributes(): array {
		return $this->optionAttributes;
	}

	public function setOutlined(bool $value = true) {
		$this->outlined = $value;
	}
}