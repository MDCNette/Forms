<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;

use MDCNette\Forms\Parts\HelperText;

interface IHelperText
{

    /**
     * @param string $message message to show under the input
     * @param bool $persistent should the message stay there or show when focus on input
     * @return $this
     */
    public function setHelperText(?string $message, bool $persistent = false, bool $validation = false);

    public function getHelperText(): ?HelperText;
}