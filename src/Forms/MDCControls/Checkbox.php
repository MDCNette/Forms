<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;


use Nette\Utils\Html;

class Checkbox extends \MDCNette\Forms\Controls\Checkbox implements ICheckbox {

	private $class = 'mdc-checkbox__native-control';

	/**
	 * @param string|object $label
	 */
	public function __construct($label = null) {
		parent::__construct($label);

	}

	/**
	 * Generates control's HTML element.
	 */
	public function getControl(): Html {
		return $this->getControlPart()->class($this->class);
	}
}