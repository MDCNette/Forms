<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;


interface ITextField extends IHelperText
{

    public function setFullwidth();

    public function setOutline();
}