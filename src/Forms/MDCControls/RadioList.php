<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;


use MDCNette\Forms\Helpers;
use Nette\Utils\Html;

class RadioList extends \MDCNette\Forms\Controls\RadioList {

	public function __construct($label = null, array $items = null) {
		parent::__construct($label, $items);
		$this->label = Html::el('div', ['class' => 'mdc-typography--body2']);
	}

	/**
	 * Generates control's HTML element.
	 */
	public function getControl(): Html {
		$input = parent::getControl();
		$items = $this->getItems();
		$ids = [];
		if ($this->generateId) {
			foreach ($items as $value => $label) {
				$ids[$value] = $input->id . '-' . $value;
			}
		}

		return $this->container->setHtml(
			Helpers::createRadioList(
				$this->translate($items),
				$this->htmlId,
				array_merge($input->attrs, [
					'class' => 'mdc-radio__native-control',
					'id:' => $ids,
					'checked?' => $this->value,
					'disabled:' => $this->disabled,
					'data-nette-rules:' => [key($items) => $input->attrs['data-nette-rules']],
				]),
				['for:' => $ids] + $this->itemLabel->attrs,
				$this->separator
			)
		);
	}

	public function setSeparator($separator) {
		$this->separator = $separator;
	}

}