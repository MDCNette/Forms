<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;


use MDCNette\Forms\Parts\HelperText;

class TextArea extends \MDCNette\Forms\Controls\TextArea
{

    /**
     * TextArea constructor.
     */
    public function __construct($label = null, int $maxLength = null)
    {
        parent::__construct($label, $maxLength);
        $this->control->class('mdc-text-field__input');
        $this->label->class('mdc-floating-label');
    }


    /**
     * @param string $message
     * @param bool $persistent
     * @return \MDCNette\Forms\Controls\TextArea
     */
    public function setHelperText(?string $message, bool $persistent = false, bool $validation = false)
    {
        $this->helperText = new HelperText($message, $persistent, $validation);
        return $this;
    }
}