<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;


use MDCNette\Forms\Helpers;
use Nette\Utils\Html;

class CheckboxList extends \MDCNette\Forms\Controls\CheckboxList implements ICheckbox {

	public function __construct($label = null, array $items = null) {
		parent::__construct($label, $items);
		$this->label = Html::el('div', ['class' => 'mdc-typography--body2']);
	}

	/**
	 * Generates control's HTML element.
	 */
	public function getControl(): Html {
		$input = parent::getControl();
		$items = $this->getItems();

		return $this->container->setHtml(
			Helpers::createCheckboxList(
				$this->translate($items),
				$this->htmlId,
				array_merge($input->attrs, [
					'class' => 'mdc-checkbox__native-control',
					'id' => null,
					'checked?' => $this->value,
					'disabled:' => $this->disabled,
					'required' => null,
					'data-nette-rules:' => [key($items) => $input->attrs['data-nette-rules']],
				]),
				$this->itemLabel->attrs,
				$this->separator
			)
		);
	}

	public function setSeparator($separator) {
		$this->separator = $separator;
	}

}