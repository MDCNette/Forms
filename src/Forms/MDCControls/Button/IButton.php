<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;


interface IButton {
	public function setOutline();

	public function setUnelevated();

	public function setRaised();

	public function setDense();
}