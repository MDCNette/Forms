<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;


use MDCNette\Forms\MDCControls\IIcon;
use MDCNette\Forms\parts\ButtonIcon;
use MDCNette\Forms\parts\Icon;
use Nette\Utils\Html;

/**
 * Class SubmitButton
 * @package MDCNette\Forms\MDCControls
 *
 * @property Icon $icon
 */
class SubmitButton extends \MDCNette\Forms\Controls\SubmitButton implements IButton, IIcon {

	protected $icon;

	public function __construct($caption = null) {
		parent::__construct($caption);
		$this->control = Html::el("button class=mdc-button data-mdc-auto-init='MDCRipple'");
		$this->control->type = 'submit';
	}

	public function setOutline() {
		$this->setOption('outline', true);
		return $this;
	}

	public function setUnelevated() {
		$this->setOption('unelevated', true);
		return $this;
	}

	public function setRaised() {
		$this->setOption('raised', true);
		return $this;
	}

	public function setDense() {
		$this->setOption('dense', true);
		return $this;
	}

	/**
	 * @param string $name name of material icon
	 * @param bool $leading flag for position of the icon
	 * @return $this
	 */
	public function setIcon(string $name, bool $leading = true) {
		$this->icon = new ButtonIcon($name, $leading);

		return $this;
	}

	public function getIcon(): ?Icon {
		return $this->icon;
	}
}