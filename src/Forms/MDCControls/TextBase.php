<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;


use MDCNette\Forms\Parts\HelperText;
use Nette\Utils\Html;

class TextBase extends \MDCNette\Forms\Controls\TextBase implements ITextField
{

    /** @var Html */
    protected $helperText;

    /**
     * TextBase constructor.
     */
    public function __construct($label, int $maxLength = null)
    {
        parent::__construct($label, $maxLength);
        if ($maxLength != null) {
            $this->setHelperText(null);
        }
    }

    public function setFullwidth()
    {
        $this->setOption('fullwidth', true);
        return $this;
    }

    public function setOutline()
    {
        $this->setOption('outline', true);
        return $this;
    }

    /**
     * @param string $message message to show under the input
     * @param bool $persistent should the message stay there or show when focus on input
     * @return $this
     */
    public function setHelperText(?string $message, bool $persistent = false, bool $validation = false)
    {
        $this->helperText = new HelperText($message, $persistent, $validation, $this->control->maxlength != null);
        return $this;
    }


    /**
     * @return string|null
     */
    public function getHelperText(): ?HelperText
    {
        return $this->helperText;
    }

    /**
     * Generates label's HTML element.
     * @param  string|object $caption
     * @return Html|string
     */
    public function getLabel($caption = null)
    {
        if ($this->caption !== null || $caption !== null) {
            $label = clone $this->label;
            $label->for = $this->getHtmlId();
            $caption = $caption === null ? $this->caption : $caption;
            $translator = $this->getForm()->getTranslator();
            $label->setText($translator ? $translator->translate($caption) : $caption);
            return $label;
        }

        return null;
    }


}