<?php

declare(strict_types=1);


namespace MDCNette\Forms\MDCControls;


interface ISelectBox {

	public function setOutlined();

}