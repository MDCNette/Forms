<?php
/**
 * MDCNette Forms
 *
 * @link        https://gitlab.com/MDCNette/Forms
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

namespace MDCNette\Forms\DI;

use MDCNette\Forms\MDCForm;
use MDCNette\Forms\Rendering\MDCFormRenderer;
use Nette;


class FormsExtension extends Nette\DI\CompilerExtension {

    public function loadConfiguration() {
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('mdcrenderer'))
            ->setClass(MDCFormRenderer::class);

        $builder->addDefinition($this->prefix('mdcform'))
            ->setClass(MDCForm::class);
    }
}
